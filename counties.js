import * as THREE from 'three';
import {OrbitControls} from "three/addons";
import * as d3 from "d3";
import GUI from 'lil-gui';
import * as TWEEN from '@tweenjs/tween.js';
import * as echarts from 'echarts';

const table_list = [ ["NaturalIncreaseRate", "SocialIncreaseRate", "PopulationIncreaseRate"],
                     ["MoveIn", "MoveOut", "SocialIncrease"],
                     ["Births", "Death", "NaturalIncrease"],
                     ["Population", "PopulationIncrease"]];
const chinese_table_list = [ ["自然增加率", "社會增加率", "人口增加率"],
                     ["移入人數", "移出人數", "社會增加數"],
                     ["出生人數", "死亡人數", "自然增加數"],
                     ["人口數", "人口增加數"]];

const color_list = ["#C3F1FF", "#FF8894", "#CCE2A3"];

let setEchartChartcreated = false;
let isMoved = false;

const echartsCharts = [];

const scene1GUI = new GUI();
const scene1GUI_obj = {
    "population": true,
    "year": 111
}
scene1GUI.add( scene1GUI_obj, "population" ).name("各縣市人口增加率");
scene1GUI.add( scene1GUI_obj, "year", 99, 111, 1 ).name("年份");
scene1GUI.show();
let selectedYear = 111;
scene1GUI.onChange( event => {
    if (event.object["population"] === true) {
        scene1.getObjectByName("Population columns group").visible = true;
        torusGroup.visible = false;
        console.log("Show population columns and hide center circles");
    } else {
        scene1.getObjectByName("Population columns group").visible = false;
        torusGroup.visible = true;
        console.log("Hide population columns and show center circles");
    }
    populationColomnsInitialRise();
    selectedYear = event.object["year"];
    console.log("Selected year:", selectedYear);
    console.log("");
} );

const backgroundColor = 0x52575F;
const countyColor = 0xC3F1FF;
const countySelectedColor = 0xFF8894;
const countyBoundaryColor = 0x00F2FF;
const populationColumnColor = 0x52D89A;
const countyThickness = 0.3;
const populationColumnInitialHeight = 0.02;

const countyIndexInJson = {"連江縣": 0, "金門縣": 1, "宜蘭縣": 2, "新竹縣": 3, "苗栗縣": 4, "彰化縣": 5, "南投縣": 6, "雲林縣": 7, "嘉義縣": 8, "屏東縣": 9, "臺東縣": 10, "花蓮縣": 11, "澎湖縣": 12, "基隆市": 13, "新竹市": 14, "嘉義市": 15, "臺北市": 16, "高雄市": 17, "新北市": 18, "臺中市": 19, "臺南市": 20, "桃園市": 21};

let scene1 = new THREE.Scene();
scene1.background = new THREE.Color( backgroundColor );

const pointer = new THREE.Vector2();
window.addEventListener( 'pointermove', onPointerMove );

let camera1 = new THREE.PerspectiveCamera(60, window.innerHeight / window.innerHeight, 0.1, 200);
camera1.position.set( 0, 12, 5 );
camera1.lookAt( 0, 0, 0 );
camera1.aspect = window.innerWidth / window.innerHeight;
camera1.updateProjectionMatrix();
scene1.add(camera1);

// const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
// scene1.add(directionalLight);
// const light = new THREE.AmbientLight(0xffffff, 0.5); // soft white light
// scene1.add(light);

const renderer = new THREE.WebGLRenderer({ alpha: true });

renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setPixelRatio(window.devicePixelRatio);

window.addEventListener("resize", () => {
    currentCamera.aspect = window.innerWidth / window.innerHeight;
    currentCamera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);
});

const container = document.getElementById("three-container");
container.appendChild(renderer.domElement);

// 初始化控制器
const controls1 = new OrbitControls(camera1, renderer.domElement);
// 设置控制器阻尼
controls1.enableDamping = true;
// 设置自动旋转
// controls1.autoRotate = true;
// controls1.addEventListener( 'change', animate );
controls1.minPolarAngle = 0;
controls1.maxPolarAngle = Math.PI * 0.5;
controls1.minDistance = 1;
controls1.maxDistance = 100;

let currentControls = controls1;
let currentCamera = camera1;
let currentScene = scene1;

// let time = {value: 0};
function animate(t) {
    currentControls.update();
    TWEEN.update(t);
    changeCountyColorWhenHover();
    currentCamera.updateProjectionMatrix();
    renderer.render(currentScene, currentCamera);
    requestAnimationFrame(animate);
}
requestAnimationFrame(animate);

// const axesHelper = new THREE.AxesHelper(100);
// scene1.add(axesHelper);
//
// const gridHelper = new THREE.GridHelper(500, 150, 0x004444, 0x004444);
// scene1.add(gridHelper);

function populationColomnsInitialRise() {
    populationColumnsGroup.children.forEach((mesh) => {
        const tween = new TWEEN.Tween({scaleY: 1, columnPositionY: countyThickness})
            .to({scaleY: populationData[mesh.name][selectedYear-99], columnPositionY: populationColumnPosition[mesh.name][selectedYear-99]}, 1700)
            .easing(TWEEN.Easing.Exponential.InOut)
            .onUpdate((parameters) => {
                mesh.scale.set(1, parameters.scaleY, 1);
                mesh.position.y = parameters.columnPositionY;
            });
        tween.start();
    });
}

function countyRiseOrFall(meshList, startScale, endScale) {
    let columnPositionY;
    if (meshList[0].type === "Mesh") {
        if (Object.keys(populationColumnPosition).length === 23) { columnPositionY = populationColumnPosition[meshList[0].name][selectedYear-99]; }
        else { columnPositionY = countyThickness; }
    }
    const tween = new TWEEN.Tween({scaleZ: startScale, columnRisePositionY: columnPositionY, columnFallPositionY: columnPositionY + countyThickness, torusDeltaPosition: 0})
        .to({scaleZ: endScale, columnRisePositionY: columnPositionY + countyThickness, columnFallPositionY: columnPositionY, torusDeltaPosition: countyThickness}, 300)
        .easing(TWEEN.Easing.Exponential.Out)
        .onUpdate((parameters) => {
            meshList.forEach((mesh) => {
                mesh.scale.set(1, 1, parameters.scaleZ);
                if (startScale < endScale) { mesh.material.color.set( countySelectedColor ); }    // county上升
                else if (startScale > endScale) {                                                 // county下降
                    if (mesh.type === "Mesh") { mesh.material.color.set( countyColor ); }
                    else if (mesh.type === "Line") { mesh.material.color.set( countyBoundaryColor ); }
                }
            });
            if ((startScale < endScale) && (meshList[0].type === "Mesh")) {
                populationColumnsGroup.children[countyIndexInJson[meshList[0].name]].position.y = parameters.columnRisePositionY;
                torusGroup.getObjectsByProperty("name", meshList[0].name).forEach(torus => {
                    torus.position.y = countyThickness + 0.01 + parameters.torusDeltaPosition;
                })
            }
            else if ((startScale > endScale) && (meshList[0].type === "Mesh")) {
                populationColumnsGroup.children[countyIndexInJson[meshList[0].name]].position.y = parameters.columnFallPositionY;
                torusGroup.getObjectsByProperty("name", meshList[0].name).forEach(torus => {
                    torus.position.y = countyThickness * 2 + 0.01 - parameters.torusDeltaPosition;
                })
            }
        });
    tween.start();
}

let countyInnerDotAndOuterRingScale = [0.02, 0.08];
function torusAnimation() {
    const tween = new TWEEN.Tween({scale: countyInnerDotAndOuterRingScale[1], opacity: 1})
        .to({scale: 0.25, opacity: 0}, 1400)
        .easing(TWEEN.Easing.Sinusoidal.InOut)
        .onUpdate((parameters) => {
            torusGroup.getObjectsByProperty("property", "Animation torus").forEach(torus => {
                torus.scale.setX(parameters.scale);
                torus.scale.setZ(parameters.scale);
                torus.material.opacity = parameters.opacity;
            })
        })
        .repeat(Infinity)
        .delay(100);
    tween.start();
}

// ----------

const loader = new THREE.FileLoader();
let jsonData;
let jsonCenter;
loader.load("/counties.json", (data) => {
    loader.load("/countyCenter.json", (center) => {
        jsonData = JSON.parse(data);
        jsonCenter = JSON.parse(center);
        operationData(jsonData, "scene1");
        boundaryCandidatesArray = scene1.getObjectsByProperty("property", "boundary");
        torusAnimation();
        (async () => {
            await getDataFromAPI();
            populationColomnsInitialRise();
            console.log("Map completely loaded.");
            console.log("");
        })();
    });
});

const projection1 = d3.geoMercator().center([120.58, 23.58]).translate([0, 0, 0]);
let projection = projection1;
let projection2;

const populationColumnsGroup = new THREE.Object3D();
const torusGroup = new THREE.Object3D();
function operationData(jsondata, scene) {
    // 全国信息
    const features = jsondata.features;

    features.forEach((feature) => {
        const boundariesGroup = new THREE.Object3D();
        const countiesGroup = new THREE.Object3D();

        const coordinates = feature.geometry.coordinates;

        if (feature.geometry.type === "MultiPolygon") {
            if (feature.properties["NAME_2014"] === "嘉義縣") {
                let coordinateCnt = 0;
                // 多个，多边形
                coordinates.forEach((coordinate) => {
                    // coordinate 多边形数据
                    if (coordinateCnt === 4) {
                        const mesh = drawExtrudeMesh(coordinate[0], countyColor, projection);
                        const mesh2 = drawExtrudeMesh(coordinate[1], backgroundColor, projection);
                        const line = lineDraw(coordinate[0], countyBoundaryColor, projection);
                        const line2 = lineDraw(coordinate[1], countyBoundaryColor, projection);
                        // 唯一标识
                        mesh.name = feature.properties["NAME_2014"];
                        mesh.scale.set(1, 1, 0.95);
                        mesh2.name = "嘉義市";
                        mesh2.material.opacity = 1;

                        boundariesGroup.add(line);
                        boundariesGroup.add(line2);
                        if (scene === "scene1") {
                            countiesGroup.add(mesh);
                            countiesGroup.add(mesh2);
                        } else if (scene === "scene2") {
                            countiesGroup.add(mesh);
                            countiesGroup.add(mesh2);
                        }
                    }
                    else {
                        coordinate.forEach((rows) => {
                            const mesh = drawExtrudeMesh(rows, countyColor, projection);
                            const line = lineDraw(rows, countyBoundaryColor, projection);
                            // 唯一标识
                            mesh.name = feature.properties["NAME_2014"];

                            boundariesGroup.add(line);
                            if (scene === "scene1") {
                                countiesGroup.add(mesh);
                            } else if (scene === "scene2") {
                                countiesGroup.add(mesh);
                            }
                        });
                    }
                    coordinateCnt += 1;
                });
            }
            else {
                coordinates.forEach((coordinate) => {
                    coordinate.forEach((rows) => {
                        const mesh = drawExtrudeMesh(rows, countyColor, projection);
                        const line = lineDraw(rows, countyBoundaryColor, projection);
                        // 唯一标识
                        mesh.name = feature.properties["NAME_2014"];

                        boundariesGroup.add(line);
                        if (scene === "scene1") {
                            countiesGroup.add(mesh);
                        } else if (scene === "scene2") {
                            countiesGroup.add(mesh);
                        }
                    });
                });
            }
        }

        if (feature.geometry.type === "Polygon") {
            // 多边形
            if (feature.properties["NAME_2014"] === "新北市") {
                // coordinates.forEach((coordinate) => {
                    const mesh = drawExtrudeMesh(coordinates[0], countyColor, projection);
                    const mesh2 = drawExtrudeMesh(coordinates[1], backgroundColor, projection);
                    const line = lineDraw(coordinates[0], countyBoundaryColor, projection);
                    const line2 = lineDraw(coordinates[1], countyBoundaryColor, projection);
                    // 唯一标识
                    mesh.name = feature.properties["NAME_2014"];
                    mesh.scale.set(1, 1, 0.95);
                    mesh2.name = "臺北市";
                    mesh2.material.opacity = 1;

                    boundariesGroup.add(line);
                    boundariesGroup.add(line2);
                    if (scene === "scene1") {
                        countiesGroup.add(mesh);
                        countiesGroup.add(mesh2);
                    } else if (scene === "scene2") {
                        countiesGroup.add(mesh);
                        countiesGroup.add(mesh2);
                    }
                // });
            }
            else {
                coordinates.forEach((coordinate) => {
                    const mesh = drawExtrudeMesh(coordinate, countyColor, projection);
                    const line = lineDraw(coordinate, countyBoundaryColor, projection);
                    // 唯一标识
                    mesh.name = feature.properties["NAME_2014"];

                    boundariesGroup.add(line);
                    if (scene === "scene1") {
                        countiesGroup.add(mesh);
                    } else if (scene === "scene2") {
                        countiesGroup.add(mesh);
                    }
                });
            }
        }

        boundariesGroup.name = feature.properties["NAME_2014"];
        boundariesGroup.property = "boundary";
        if (scene === "scene1") {
            const squareColumnSideLength = 0.1;
            const geometryBirth = new THREE.BoxGeometry( squareColumnSideLength, populationColumnInitialHeight, squareColumnSideLength );
            geometryBirth.translate( (jsonCenter[feature.properties["NAME_2014"]][1] - 120.58) * 2.69, 0, (23.57 - jsonCenter[feature.properties["NAME_2014"]][0]) * 2.93 );
            const materialBirth = new THREE.MeshBasicMaterial( {color: populationColumnColor} );
            const meshBirth = new THREE.Mesh( geometryBirth, materialBirth );
            meshBirth.position.y = populationColumnInitialHeight / 2 + countyThickness;
            meshBirth.name = feature.properties["NAME_2014"];
            meshBirth.property = "Population column";
            populationColumnsGroup.add( meshBirth );

            const torusGeometry = new THREE.TorusGeometry( 1, 0.05, 2, 100 );
            const centerTorusGeometry = new THREE.TorusGeometry( 1, 1, 2, 100 );
            torusGeometry.rotateX(-Math.PI/2);
            centerTorusGeometry.rotateX(-Math.PI/2);
            const animationTorusMaterial = new THREE.MeshBasicMaterial( { color: 0xffff00 } );
            const fixedTorusMaterial = new THREE.MeshBasicMaterial( { color: 0xffff00 } );
            animationTorusMaterial.transparent = true;
            fixedTorusMaterial.transparent = true;
            const animationTorus = new THREE.Mesh( torusGeometry, animationTorusMaterial );
            const centerTorus = new THREE.Mesh( centerTorusGeometry, fixedTorusMaterial );
            const ringTorus = new THREE.Mesh( torusGeometry, fixedTorusMaterial );
            animationTorus.position.set((jsonCenter[feature.properties["NAME_2014"]][1] - 120.58) * 2.69, countyThickness + 0.01, (23.57 - jsonCenter[feature.properties["NAME_2014"]][0]) * 2.93 );
            centerTorus.position.set((jsonCenter[feature.properties["NAME_2014"]][1] - 120.58) * 2.69, countyThickness + 0.01, (23.57 - jsonCenter[feature.properties["NAME_2014"]][0]) * 2.93 );
            ringTorus.position.set((jsonCenter[feature.properties["NAME_2014"]][1] - 120.58) * 2.69, countyThickness + 0.01, (23.57 - jsonCenter[feature.properties["NAME_2014"]][0]) * 2.93 );
            animationTorus.scale.set(countyInnerDotAndOuterRingScale[1], 1, countyInnerDotAndOuterRingScale[1]);
            centerTorus.scale.set(countyInnerDotAndOuterRingScale[0], 1, countyInnerDotAndOuterRingScale[0]);
            ringTorus.scale.set(countyInnerDotAndOuterRingScale[1], 1, countyInnerDotAndOuterRingScale[1]);
            animationTorus.name = feature.properties["NAME_2014"];
            centerTorus.name = feature.properties["NAME_2014"];
            ringTorus.name = feature.properties["NAME_2014"];
            animationTorus.property = "Animation torus";
            centerTorus.property = "Center torus";
            ringTorus.property = "Ring torus";
            animationTorus.renderOrder = 5;
            centerTorus.renderOrder = 3;
            ringTorus.renderOrder = 4;
            torusGroup.add(animationTorus);
            torusGroup.add(centerTorus);
            torusGroup.add(ringTorus);
            scene1.add(torusGroup);
            torusGroup.visible = false;

            scene1.add(boundariesGroup);
            countiesGroup.name = feature.properties["NAME_2014"];
            countiesGroup.property = "county";
            scene1.add(countiesGroup);
        } else if (scene === "scene2") {
            scene2.add(boundariesGroup);
            countiesGroup.name = feature.properties["NAME_2014"];
            countiesGroup.property = "county";
            scene2.add(countiesGroup);
        }
        console.log(feature.properties["NAME_2014"], "loaded");
    });
    populationColumnsGroup.name = "Population columns group";
    populationColumnsGroup.property = "Square columns";
    torusGroup.name = "Torus group";
    torusGroup.property = "Torus group";
    scene1.add(populationColumnsGroup);
    scene1.add(torusGroup);
}

function lineDraw(polygon, color, projection_) {
    const lineGeometry = new THREE.BufferGeometry();
    const pointsArray = new Array();
    polygon.forEach((row) => {
        const [x, y] = projection_(row);
        // 创建三维点
        pointsArray.push(new THREE.Vector3(x, -y, countyThickness + 0.00001));
    });
    // 放入多个点
    lineGeometry.setFromPoints(pointsArray);

    const lineMaterial = new THREE.LineBasicMaterial({
        color: color
    });

    const lines = new THREE.Line(lineGeometry, lineMaterial);
    lines.rotateX(-Math.PI/2);
    return lines;
}

// 根据经纬度坐标生成物体
function drawExtrudeMesh(polygon, color, projection_) {
    const shape = new THREE.Shape();
    polygon.forEach((row, i) => {
        const [x, y] = projection_(row);
        if (i === 0) {
            // 创建起点,使用moveTo方法
            // 因为计算出来的y是反过来，所以要进行颠倒
            shape.moveTo(x, -y);
        }
        shape.lineTo(x, -y);
    });

    // 縣市多邊形方塊本體
    const geometry = new THREE.ExtrudeGeometry(shape, {
        depth: countyThickness,
        bevelEnabled: false,
    });

    // 縣市多邊形方塊本體
    const material = new THREE.MeshBasicMaterial({
        color: color,
        transparent: true,
        opacity: 0.4,
    });

    const extrudeMesh = new THREE.Mesh(geometry, material);
    extrudeMesh.rotateX(-Math.PI/2);
    return extrudeMesh;
}

function onPointerMove( event ) {
    // calculate pointer position in normalized device coordinates
    // (-1 to +1) for both components
    pointer.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    pointer.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}

let selectedCountyName = null;
let countyCandidatesArray = [];
let boundaryCandidatesArray = [];
function changeCountyColorWhenHover() {
    const rayCaster = new THREE.Raycaster();
    rayCaster.setFromCamera( pointer, currentCamera );

    let intersects = [];
    if (currentScene === scene1){
        countyCandidatesArray = scene1.getObjectsByProperty("property", "county");
        intersects = rayCaster.intersectObjects( countyCandidatesArray );
        if (intersects.length !== 0){
            if (intersects[0].object.name !== selectedCountyName) {
                countyRiseOrFall(countyCandidatesArray[countyIndexInJson[intersects[0].object.name]].children, 1, 2);
                countyRiseOrFall(boundaryCandidatesArray[countyIndexInJson[intersects[0].object.name]].children, 1, 2);
                console.log("Hover", intersects[0].object.name);
                if (selectedCountyName !== null) {
                    countyRiseOrFall(countyCandidatesArray[countyIndexInJson[selectedCountyName]].children, 2, 1);
                    countyRiseOrFall(boundaryCandidatesArray[countyIndexInJson[selectedCountyName]].children, 2, 1);
                }
                selectedCountyName = intersects[0].object.name;
            }
        } else {
            if (selectedCountyName !== null) {
                countyRiseOrFall(countyCandidatesArray[countyIndexInJson[selectedCountyName]].children, 2, 1);
                countyRiseOrFall(boundaryCandidatesArray[countyIndexInJson[selectedCountyName]].children, 2, 1);
                selectedCountyName = null;
            }
        }
    }
    return intersects;
}

window.addEventListener("mousedown", (clickCounty) => {
    const rayCaster = new THREE.Raycaster();
    rayCaster.setFromCamera(pointer, currentCamera);

    if (currentScene === scene1) {
        const clickedCounty = rayCaster.intersectObjects(scene1.getObjectsByProperty("property", "county"));

        // Move the setEchartChartcreated variable to the outer scope
        const d3Container = d3.select("#d3-container");
        d3Container.style("display", "none");

        currentCamera.userData.enabled = true;
        scene1.visible = true;

        clickedCounty.forEach(county => {
            if (currentScene === scene1) {
                console.log("Clicked", county.object.name);

                // Clear the content of scene2
                scene2.clear();

                async function fetchData(table, countyName, tempData) {
                    try {
                        const tmpData = await d3.json(`/api/${table}/${countyName}`);
                        console.log("table", table);
                
                        tempData.push(tmpData);
                    } catch (error) {
                        console.error("Error fetching data:", error);
                    }
                }
                
                jsonData.features.forEach(async feature => {
                    if (feature.properties["NAME_2014"] === county.object.name) {
                        // Hide the D3 container
                        d3Container.style("display", "none");

                        const data = [];
                        const promises = [];
                
                        for (const table_index of table_list) {
                            const tempData = []; // Create tempData array here
                            for (const table of table_index) {
                                await fetchData(table, county.object.name, tempData);
                                console.log("tempData", tempData);
                            }
                            // Add the entire tempData array to the data array
                            data.push(tempData);
                        }
                
                        // Code to execute after all asynchronous operations are completed
                        if (!setEchartChartcreated) {
                            console.log("Inspecting", county.object.name, "at Scene 2.");
                            console.log("");
                            console.log("data", data);


                            // Set background image for the entire body
                            document.body.style.backgroundImage = `url('/各縣市截圖/${county.object.name}.png')`;
                            document.body.style.backgroundSize = 'cover';
                            document.body.style.backgroundPosition = 'center center';
                            document.body.style.backgroundAttachment = 'fixed';
                            document.body.style.padding = "10"; // Set margin to 0
                            document.body.style.height = window.innerHeight + 'px';
                            document.body.style.fontFamily = 'SimSun, sans-serif';

                            // Create navigation bar
                            const navigationBar = document.createElement('div');
                            navigationBar.id = 'NavigationBar';
                            navigationBar.style.backgroundColor = '#333'; // Background color of the navigation bar
                            navigationBar.style.color = '#fff'; // Text color
                            navigationBar.style.padding = '10px'; // Padding for the content inside the navigation bar
                            navigationBar.style.textAlign = 'center'; // Center-align the content
                            navigationBar.style.fontSize = '30px'; 
                            navigationBar.style.position = 'fixed'; 
                            navigationBar.style.top = '0'; // Set top to 0 to fix it at the top
                            navigationBar.style.width = '100%'; // Set the width to 100% to cover the entire width
                            navigationBar.style.zIndex = '1000'; // Set a high z-index to ensure it's on top
                            
                            // Set the content of the navigation bar to the county name
                            navigationBar.textContent = county.object.name;
                            
                            // Append navigationBar to the body
                            document.body.appendChild(navigationBar);
                            
                            createStackedBarChart(data, 0);
                            createStreamGraph(data, 1);
                            createStreamGraph(data, 2);
                            createDualAxisBarLineChart(data, 3);
                
                            setEchartChartcreated = true;
                            scene2.visible = true;
                        }
                
                        console.log("Inspecting", county.object.name, "at Scene 2.");
                        console.log("");

                        
                        const canvas = d3.select("canvas");
                        canvas.style("display", "none");
                
                        // Update GUI visibility
                        scene1GUI.hide();
                        scene2GUI.show();
                        // scene2GUI.close();

                        const moveButton = document.getElementById('NavigationBar');

                        console.log(echartsCharts);
                        
                        moveButton.addEventListener('click', () => {
                            let i = 0;
                            console.log('click');
                            // 遍歷 echartsCharts 數組
                            const startIndex = Math.max(0, echartsCharts.length - 4); // 確保 startIndex 不會小於零
                            const lastFourCharts = echartsCharts.slice(startIndex);

                            lastFourCharts.forEach((echartsChart, index) => {

                                // 根據當前狀態調整圖表位置
                                moveChartContainer(echartsChart, index);
                            });

                        
                            // 切換狀態
                            isMoved = !isMoved;
                        });

                    }
                });                
            }
        });
    }
});

let populationColumnPosition = {};
let populationData = {};
async function getDataFromAPI() {
    const rawDataCountyIndex = ["總計", "新北市", "臺北市", "桃園市", "臺中市", "臺南市", "高雄市", "宜蘭縣", "新竹縣", "苗栗縣", "彰化縣", "南投縣", "雲林縣", "嘉義縣", "屏東縣", "臺東縣", "花蓮縣", "澎湖縣", "基隆市", "新竹市", "嘉義市", "金門縣", "連江縣"];
    
    async function fetchRawData() {
        try {
            const tmpData = await d3.json(`/api/PopulationIncreaseRate`);
            const offset = calculateOffsets(tmpData);

            const Data = applyOffsets(tmpData, offset);

            // const standardizeData = standardizeData(Data);

            return Data;
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    }

    try {
        const rawData = await fetchRawData();
        console.log(rawData);
        for (let i = 0; i < rawDataCountyIndex.length; i++) {
            populationData[rawDataCountyIndex[i]] = [];
            populationColumnPosition[rawDataCountyIndex[i]] = [];
            for (let j = 0; j < rawData[0].length; j++) {
                populationData[rawDataCountyIndex[i]][j] = parseInt(rawData[i][j]);
                populationColumnPosition[rawDataCountyIndex[i]][j] = populationColumnInitialHeight * parseInt(rawData[i][j]) / 2 + countyThickness;
            }
        }
        console.log("Population data:\n", populationData);
    } catch (error) {
        console.error("Error processing data:", error);
    }
}

    const scene2GUI = new GUI();
    const scene2GUI_obj = {
        backToScene1() {
            currentControls = controls1;
            currentScene = scene1;
            currentCamera = camera1;
            scene1GUI.show();
            scene2GUI.hide();

            const navigationBarToRemove = document.getElementById('NavigationBar');

            if (navigationBarToRemove) {
                // 從其父節點中刪除 navigationBar
                navigationBarToRemove.parentNode.removeChild(navigationBarToRemove);
            }            
            

            const d3Container = d3.select("#d3-container");
            d3Container.style("display", "none");
            const canvas = d3.select("canvas");
            canvas.style("display", "block");


            
            controls1.enabled = true;
            currentCamera.userData.enabled = true;
            scene1.visible = true;
            setEchartChartcreated = false;

            scene2CountiesGroup.children.forEach(child => {
                scene2CountiesGroup.remove(child);
            });

            const echartsContainers = document.querySelectorAll('#echarts-container');

            echartsCharts.forEach(echartsItem => {
                // 取得 ECharts 實例
                const echartsChart = echartsItem.chart;
            
                // 釋放 ECharts 實例
                echarts.dispose(echartsChart);
            });

            // 遍历所有 ECharts 容器并移除
            echartsContainers.forEach(echartsContainer => {
                echartsContainer.parentNode.removeChild(echartsContainer);
            });

            console.log(echartsCharts);
            echartsCharts = [];
            console.log(echartsCharts);


            console.log("Control button clicked. Back to Scene 1.");
            console.log("");
        }
    }
    scene2GUI.add( scene2GUI_obj, "backToScene1" ).name("Go back to Scene 1");
    scene2GUI.hide();

// const tween = new TWEEN.Tween();

function disposeAllCharts() {
    for (const chart of echartsCharts) {
        chart.dispose(); // Dispose of the ECharts instance
    }

    // Remove the HTML div elements from the DOM
    for (const chartContainer of document.querySelectorAll('.echarts-container')) {
        chartContainer.remove();
    }

    // Clear the echartsCharts array
    echartsCharts.length = 0;
}

// -----------------------------------------------
let scene2 = new THREE.Scene();
// scene2.background = new THREE.Color( backgroundColor );
const scene2CountiesGroup = new THREE.Group();
scene2.add(scene2CountiesGroup);

let camera2 = new THREE.PerspectiveCamera(30, window.innerHeight / window.innerHeight, 0.001, 500);

// 更新摄像头
camera2.aspect = window.innerWidth / window.innerHeight;
//   更新摄像机的投影矩阵
camera2.updateProjectionMatrix();
scene2.add(camera2);


function createStreamGraph(data, index) {
    // Create a new div for each chart
    const echartsContainer = document.createElement('div');
    echartsContainer.style.width = `${window.innerWidth * 0.34}px`;
    echartsContainer.style.height = `${window.innerHeight * 0.3}px`;
    echartsContainer.id = `echarts-container`

    // Position the first stream graph in the top-right quadrant
    if (index === 1) {
        echartsContainer.style.position = 'absolute';
        echartsContainer.style.top = '90px';
        echartsContainer.style.right = '30px';
    }
    // Position the second stream graph in the bottom-left quadrant
    else if (index === 2) {
        echartsContainer.style.position = 'absolute';
        echartsContainer.style.bottom = '30px';
        echartsContainer.style.left = '30px';
    }

    document.body.appendChild(echartsContainer);

    // Create a new ECharts instance for each chart
    const echartsChart = echarts.init(echartsContainer);
    echartsContainer.style.border = '2px solid white';
    echartsCharts.push({
        chart: echartsChart,
        originalPosition: {
        position: echartsContainer.style.position,
        top: echartsContainer.style.top,
        left: echartsContainer.style.left,
        right: echartsContainer.style.right,
        bottom: echartsContainer.style.bottom,
        transform: echartsContainer.style.transform,
    },
    });

    const xAxisData = Array.from({ length: 13 }, (_, i) => 99 + i);

    // const Data = data.map(row => row.map(value => parseFloat(value.replace("–", "-"))));

    let echartsOption = {
        backgroundColor:"#97A0B1",
        xAxis: {
            type: 'category',
            data: xAxisData,
            axisLine: {
                lineStyle: {
                    color: backgroundColor, // Change the color to white
                },
            },
            axisLabel: {
                color: backgroundColor, // Make the label text transparent
                rich: {
                    label: {
                        color: backgroundColor, // Color for the label part
                        align: 'center', // Center align the label
                        verticalAlign: 'top',
                    },
                },
            },
        },
        yAxis: {
            type: 'value',
            scale: true,
            axisLabel: {
                color: backgroundColor, // Make the label text transparent
                rich: {
                    label: {
                        color: backgroundColor, // Color for the label part
                        align: 'center', // Center align the label
                        verticalAlign: 'top',
                    },
                },
            },
        },
        legend: {
            data: [chinese_table_list[index][0], chinese_table_list[index][1], chinese_table_list[index][2]],
            bottom:0,
        },
        series: [
            {
                type: 'line',
                name: chinese_table_list[index][0],
                areaStyle: {},
                color: color_list[0],
                data: data[index][0],
            },
            {
                type: 'line',
                name: chinese_table_list[index][1],
                areaStyle: {},
                color: color_list[1],
                data: data[index][1],
            },
            {
                type: 'line',
                name: chinese_table_list[index][2],
                areaStyle: {},
                color: color_list[2],
                data: data[index][2],
            },
        ],
        // Add the following click event to handle legend item clicks
        // It toggles the visibility of the clicked series
        tooltip: {
            trigger: 'axis',
        },
        toolbox: {
            feature: {
                saveAsImage: {},
            },
        },
        grid: {
            top: '20%',
            left: '4%',
            right: '8%',
            bottom: '15%',
            containLabel: true,
        },
    };

     // Position the first stream graph in the top-right quadrant
     if (index === 1) {
        echartsOption.title = {
            text: '社會增加數', // 標題文本
            textStyle: {
              fontSize: 16, // 標題字體大小
              fontWeight: 'bold', // 標題字體粗細
              color: "white",
            },
            padding: 10, // 標題內側間距
        };
    }

    // Position the second stream graph in the bottom-left quadrant
    else if (index === 2) {
        echartsOption.title = {
            text: '自然增加數', // 標題文本
            textStyle: {
              fontSize: 16, // 標題字體大小
              fontWeight: 'bold', // 標題字體粗細
              color: "white",
            },
            padding: 10, // 標題內側間距
        };
    }
    

    echartsChart.setOption(echartsOption);

    const imgDataUrl = echartsChart.getDataURL();

    const texture = new THREE.TextureLoader().load(imgDataUrl);

    const plane = new THREE.Mesh(new THREE.PlaneGeometry(8, 6), new THREE.MeshBasicMaterial({ map: texture }));

    // Adjust the position of the plane
    plane.position.set(100, 200, 0); // Replace xPosition, yPosition, zPosition with your desired coordinates

    scene2CountiesGroup.add(plane);
}


function createStackedBarChart(data, index) {
    // Create a new div for each chart
    const echartsContainer = document.createElement('div');
    echartsContainer.style.width = `${window.innerWidth * 0.34}px`;
    echartsContainer.style.height = `${window.innerHeight * 0.3}px`;
    echartsContainer.id = 'echarts-container';

    // Position in the top-left quadrant
    echartsContainer.style.position = 'absolute';
    echartsContainer.style.top = '90px';
    echartsContainer.style.left = '30px';

    document.body.appendChild(echartsContainer);

    // Create a new ECharts instance for each chart
    const echartsChart = echarts.init(echartsContainer);
    echartsContainer.style.border = '2px solid white';
    echartsCharts.push({
        chart: echartsChart,
        originalPosition: {
            position: echartsContainer.style.position,
            top: echartsContainer.style.top,
            left: echartsContainer.style.left,
            right: echartsContainer.style.right,
            bottom: echartsContainer.style.bottom,
            transform: echartsContainer.style.transform,
        },
    });

    const xAxisData = Array.from({ length: 13 }, (_, i) => 99 + i);

    const echartsOption = {
        backgroundColor:"#97A0B1",
        title: {
            text: '人口增加率', // 標題文本
            textStyle: {
              fontSize: 16, // 標題字體大小
              fontWeight: 'bold', // 標題字體粗細
              color: "white",
            },
            padding: 10, // 標題內側間距
          },
        xAxis: {
            type: 'category',
            data: xAxisData,
            axisLine: {
                lineStyle: {
                    color: backgroundColor, // Change the color to white
                },
            },
            axisLabel: {
                color: backgroundColor, // Make the label text transparent
                rich: {
                    label: {
                        color: backgroundColor, // Color for the label part
                        align: 'center', // Center align the label
                        verticalAlign: 'top',
                    },
                },
            },
        },
        yAxis: {
            type: 'value',
            scale: true,
            axisLabel: {
                color: backgroundColor, // Make the label text transparent
                rich: {
                    label: {
                        color: backgroundColor, // Color for the label part
                        align: 'center', // Center align the label
                        verticalAlign: 'top',
                    },
                },
            },
        },
        legend: {
            data: [chinese_table_list[index][0], chinese_table_list[index][1], chinese_table_list[index][2]],
            bottom:0,
        },
        series: [
            {
                type: 'bar',
                name: chinese_table_list[index][0],
                stack: 'stacked',
                color: color_list[0],
                data: data[index][0],
            },
            {
                type: 'bar',
                name: chinese_table_list[index][1],
                stack: 'stacked',
                color: color_list[1],
                data: data[index][1],
            },
            {
                type: 'line',
                name: chinese_table_list[index][2],
                color: color_list[2],
                data: data[index][2],
            },
        ],
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow', // Show tooltip at the axis' edge
            },
        },
        toolbox: {
            feature: {
                saveAsImage: {},
            },
        },
        grid: {
            top: '20%',
            left: '4%',
            right: '8%',
            bottom: '15%',
            containLabel: true,
        },
    };

    echartsChart.setOption(echartsOption);

    const imgDataUrl = echartsChart.getDataURL();

    const texture = new THREE.TextureLoader().load(imgDataUrl);

    const plane = new THREE.Mesh(new THREE.PlaneGeometry(8, 6), new THREE.MeshBasicMaterial({ map: texture }));

    scene2CountiesGroup.add(plane);
}


function createDualAxisBarLineChart(data, index) {
    // Create a new div for each chart
    const echartsContainer = document.createElement('div');
    echartsContainer.style.width = `${window.innerWidth * 0.34}px`;
    echartsContainer.style.height = `${window.innerHeight * 0.3}px`;
    echartsContainer.id = 'echarts-container';

    // Position in the bottom-left quadrant
    echartsContainer.style.position = 'absolute';
    echartsContainer.style.bottom = '30px';
    echartsContainer.style.right = '30px';

    document.body.appendChild(echartsContainer);

    // Create a new ECharts instance for each chart
    const echartsChart = echarts.init(echartsContainer);
    echartsContainer.style.border = '2px solid white';
    echartsCharts.push({
        chart: echartsChart,
        originalPosition: {
        position: echartsContainer.style.position,
        top: echartsContainer.style.top,
        left: echartsContainer.style.left,
        right: echartsContainer.style.right,
        bottom: echartsContainer.style.bottom,
        transform: echartsContainer.style.transform,
    },
    });
    const xAxisData = Array.from({ length: 13 }, (_, i) => 99 + i);

    const echartsOption = {
        title: {
            text: '人口數與人口增加數', // 標題文本
            textStyle: {
              fontSize: 16, // 標題字體大小
              fontWeight: 'bold', // 標題字體粗細
              color: "white",
            },
            padding: 10, // 標題內側間距
          },
        backgroundColor:"#97A0B1",
        xAxis: {
            type: 'category',
            data: xAxisData,
            axisLine: {
                lineStyle: {
                    color: backgroundColor, // Change the color to white
                },
            },
        },
        yAxis: [
            {
                type: 'value',
                scale: true,
                data: data[index][0],
                axisLine: {
                    lineStyle: {
                        color: backgroundColor, // Change the color to white
                    },
                },
                // axisLabel: {
                //     fontSize: 8, // Set the font size
                //     color: backgroundColor, // Change the color to white
                // },
            },
            {
                type: 'value',
                scale: true,
                data: data[index][1],
                axisLine: {
                    lineStyle: {
                        color: backgroundColor, // Change the color to white
                    },
                },
                // axisLabel: {
                //     fontSize: 8, // Set the font size
                //     color: backgroundColor, // Change the color to white
                // },
            },
        ],
        legend: {
            data: [chinese_table_list[index][0], chinese_table_list[index][1]],
            bottom: 0,
        },
        series: [
            {
                type: 'bar',
                name: chinese_table_list[index][0],
                data: data[index][0],
                color: color_list[0],
                yAxisIndex: 0, // Plot on the first y-axis
            },

            {
                type: 'line',
                name: chinese_table_list[index][1],
                data: data[index][1],
                color: color_list[1],
                yAxisIndex: 1, // Plot on the second y-axis
            },
        ],
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow', // Show tooltip at the axis' edge
            },
        },
        toolbox: {
            feature: {
                saveAsImage: {},
            },
        },
        grid: {
            top: '20%',
            left: '3%',
            right: '4%',
            bottom: '15%',
            containLabel: true,
        },
    };

    echartsChart.setOption(echartsOption);
}


function moveChartContainer(echartsChart, index) {
    const container = echartsChart.chart.getDom(); // 獲取 ECharts 容器的 DOM 元素

    if (isMoved) {
        // 移動回原始位置
        document.body.style.height = window.innerHeight + 'px';
        container.style.position =echartsChart.originalPosition.position;
        container.style.top =echartsChart.originalPosition.top;
        container.style.left =echartsChart.originalPosition.left;
        container.style.right =echartsChart.originalPosition.right;
        container.style.bottom =echartsChart.originalPosition.bottom;
        container.style.transform =echartsChart.originalPosition.transform;
    } else {
        document.body.style.height= '200vh'; // Set margin to 0
        document.addEventListener('scroll', function(event) {
            // 計算滾動距離並根據需要調整背景位置
            const scrollDistance = window.scrollY;
            document.body.style.backgroundPosition = `0 ${scrollDistance}px`;
          });
        // 根據 index 分別處理位置
        switch (index) {
            case 0: // 中間偏左上
                container.style.position = 'absolute';
                container.style.top = '20%';
                container.style.left = '15%';
                // container.style.transform = 'translate(-50%, -50%)';
                break;
            case 1: // 中間偏右上
                container.style.position = 'absolute';
                container.style.top = '20%';
                container.style.right = '15%';
                // container.style.transform = 'translate(50%, -50%)';
                break;
            case 2: // 中間偏左下
                container.style.position = 'absolute';
                container.style.bottom = '15%';
                container.style.left = '15%';
                // container.style.transform = 'translate(-50%, 50%)';
                break;
            case 3: // 中間偏右下
                container.style.position = 'absolute';
                container.style.bottom = '15%';
                container.style.right = '15%';
                // container.style.transform = 'translate(50%, 50%)';
                break;
            default:
                // 中間偏左上作為默認值
                container.style.position = 'absolute';
                container.style.top = '20%';
                container.style.left = '20%';
        }
    }
}

// 偏移量计算
// 将字符串数组转换为数字数组
function convertToNumbers(stringArray) {
    return stringArray.map(value => parseFloat(value));
  }
  
// 计算数组中的最小值
function calculateMinValue(array) {
    return Math.min(...array);
  }
  
// 计算所有数组的偏移量
function calculateOffsets(arrays) {
    return arrays.map(array => calculateMinValue(convertToNumbers(array)));
  }
  
// 应用偏移量
function applyOffsets(arrays, offsets) {
    // 找到全部数组中的最小值
    const minValue = arrays.reduce((min, array) => {
      const arrayMin = Math.min(...convertToNumbers(array));
      return arrayMin < min ? arrayMin : min;
    }, Infinity);
  
    // 对每个数组进行偏移
    return arrays.map((array, index) =>
      convertToNumbers(array).map(value => value - minValue)
    );
}

function standardizeData(data) {
    const standardizedData = {};
  
    for (const key in data) {
      const values = data[key];
      const mean = values.reduce((acc, val) => acc + val, 0) / values.length;
      const stdDev = Math.sqrt(values.reduce((acc, val) => acc + Math.pow(val - mean, 2), 0) / values.length);
  
      standardizedData[key] = values.map(value => (value - mean) / stdDev);
    }
  
    return standardizedData;
}
