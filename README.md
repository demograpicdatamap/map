# Map

### Theme
Visualizing population and its variation over the years in counties in Taiwan with a 3D map on the web.

### Application technique
- Front-end
    - Three.js ➡ Visualizing population data with a 3D map
    - ECharts ➡ Showing graphs and charts of historical population data
- Back-end
    - MariaDB ➡ Storage for counties' population data
    - FastAPI ➡ Front-end retrieves data from database with Back-end's API

### Repositories
- Front-end web: https://gitlab.com/demograpicdatamap/map
- Back-end API: https://gitlab.com/demograpicdatamap/mapapi

### Demo website
https://www.blackboard.services/ (Valid before 2024/01/06)

<br/>

---

<br/>

* 主題：在網頁上以3D地圖呈現各縣市的人口資料與變化

* 網頁呈現
    * 前端
        * Three.js ➡ 網站首頁地圖呈現
        * ECharts ➡ 以圖表呈現各縣市之歷年人口資料
    * 後端
        * MariaDB ➡ 以資料庫儲存各縣市歷年人口資料
        * FastAPI ➡ 以API串連前後端，使前端可以獲取資料庫中的資料

* 原始碼
    * 前端網頁：https://gitlab.com/demograpicdatamap/map
    * 後端API：https://gitlab.com/demograpicdatamap/mapapi

* Demo網址（2024/01/06前有效）： https://www.blackboard.services/
